#-----Statement of Authorship----------------------------------------#
#
#  By submitting this task the signatories below agree that it
#  represents our own work and that we both contributed to it.  We
#  are aware of the University rule that a student must not
#  act in a manner which constitutes academic dishonesty as stated
#  and explained in QUT's Manual of Policies and Procedures,
#  Section C/5.3 "Academic Integrity" and Section E/2.1 "Student
#  Code of Conduct".
#
#  First student's no: n8824088
#  First student's name: Scott Schultz
#  Portfolio contribution: 100%
#
#  Second student's no: Solo Submission
#
#  Contribution percentages refer to the whole portfolio, not just this
#  task.  Percentage contributions should sum to 100%.  A 50/50 split is
#  NOT necessarily expected.  The percentages will not affect your marks
#  except in EXTREME cases.
#
#--------------------------------------------------------------------#


#-----Task Description-----------------------------------------------#
#
#  NUCLEAR FUSION
#
#  In this task you will create a simple simulation using the PyGame
#  package.  The program will animate the behavour of a collection
#  of atoms undergoing nuclear fusion.  The atoms should randomly
#  bounce around the screen but when two collide they fuse, leaving
#  a single atom and some residual energy.  Since this will
#  rapidly exhaust the supply of atoms, clicking the mouse in the
#  screen should add a new atome to the reaction.  See the instructions
#  accompanying this file for full detail.
#  
#--------------------------------------------------------------------#


#-----Students' Solution---------------------------------------------#
#
#  Complete the task by modifying or replacing the Bouncing Ball
#  demonstration program below.

#### MODIFY/REPLACE THE FOLLOWING CODE

#  This small program demonstrates the main features of the
#  PyGame package.  The code is uncommented, but is explained in
#  detail in the accompanying instruction sheet.  (Your solution
#  must be well commented, of course.)

#--------------------------------------------------------------------#
#                               Imports                              #
#--------------------------------------------------------------------#

# We want the whole pygame library, though I prefer to be able to use the library name before the functions so I don't get mixed up, so I used import instead of
# from pygame import *
import pygame
# I only need the rantint() and uniform() functions from the random library, so there's no need to import the entire library.
from random import randint,uniform
# The functions are sort of self-explanitory, uniform is maybe a bit ambiguous, its basically randint() for float values. Specifically I want floating numbers <1.0

#--------------------------------------------------------------------#
#                              Constants                             #
#--------------------------------------------------------------------#

explosion_image_list = [
	'data/images/explosion_1.png',
	'data/images/explosion_2.png',
	'data/images/explosion_3.png'
]

# joke audio, overwritten with next list
explosion_audio_list = [
	'data/audio/EXPLOSION_NOISE_here.ogg',
	'data/audio/EXPLOOOOSIOOOONS.ogg'
]
explosion_audio_list = [
	'data/audio/explosion_1.wav',
	'data/audio/explosion_2.wav',
	'data/audio/explosion_3.wav',
	'data/audio/explosion_4.wav'
]
wall_collision_audio_list = [
	'data/audio/tink_1.wav',
	'data/audio/tink_2.wav',
	'data/audio/tink_3.wav',
	'data/audio/tink_4.wav',
	'data/audio/tink_5.wav'
]
atom_image_list = [
	'data/images/atom_1.png',
	'data/images/atom_2.png'
]

WIDTH = 1000
HEIGHT = 900
DELAY = 20

BORDER = 20
BACKGROUND_COLOUR = (100, 100, 100) # dark grey tuple
CAPTION = 'EXPLOSIONS - Scott Schultz n8824088'

#--------------------------------------------------------------------#
#                               Globals                              #
#--------------------------------------------------------------------#

atom_pos_list = []
explosion_pos_list = []
audio_list = []
bounce_audio_list = []

pygame.mixer.pre_init(37000, -16, 2)
pygame.init()
pygame.display.set_caption(CAPTION)
frame = pygame.display.set_mode((WIDTH, HEIGHT))

def load_audio():
	"""
	Creates and populates audio lists for use in later functions
	"""

    # Populates the global audio lists with explosion and wall collision sounds, regardless of how relevant they may be to the actual sound

	for explosion_sound in explosion_audio_list:
		audio_list.append(pygame.mixer.Sound(explosion_sound))
	for wall_collision_sound in wall_collision_audio_list:
		bounce_audio_list.append(pygame.mixer.Sound(wall_collision_sound))

# Define our arguments as False so that they are not forced for calling the function
def create_new_atom(spawn_x = False,spawn_y = False):
	"""
	This function handles both creating the initial atoms, as well as acting as the function that spawns new atoms at a collision point.

	If spawn_x AND spawn_y are not given a value, the function will create a random atom anywhere on the screen. If the function receives both arguments,
	an atom is created at that point.

	If the function is only given 1 argument, the atom will be spawned in a random location on the screen as well.
	"""

	# Generate a random x and y value, get the atom image, define its rectangle using the generated x and y co-ords, then give it a random x and y velocity
	# a velocity of 0,0 is a valid velocity too, even if the atom doesn't move.
	random_pos_x = randint(100,WIDTH-100)
	random_pos_y = randint(100,HEIGHT-100)

	# lets make the atom a random image, why not
	# pick a random number from 0 to 1 less than the number of atom images referenced within the atom_image_list
	random_atom_image_index = randint(0,len(atom_image_list)-1)

	# load an atom image into memory based on the random number chosen above, then use convert_alpha() to preserve transarency
	atom_sprite = pygame.image.load(atom_image_list[random_atom_image_index]).convert_alpha()

	# And while we're at it, lets give the random atom a random scale to make things interesting.
	# also the images could be any size, so grab the original size of the image loaded, and shrink them down to somewhere between 20% and 50% of their size,
	# or 0.2 and 0.5 as floats, respectively
	w,h = atom_sprite.get_size()
	scale = uniform(0.2,0.5)

	# Now apply the scale transformation to our loaded and converted image
	atom_sprite = pygame.transform.scale(atom_sprite, (int(w*scale), int(h*scale)))

	# then load that atom into our list
	if spawn_x and spawn_y:
		atom_pos_list.append([atom_sprite, atom_sprite.get_rect(center = (spawn_x,spawn_y)),[randint(-10,10),randint(-10,10)]])
	else:
		atom_pos_list.append([atom_sprite, atom_sprite.get_rect(center = (random_pos_x,random_pos_y)),[randint(-10,10),randint(-10,10)]])

def move_atoms(atom_list):
	"""
	This function handles moving and updating the position of atoms within the atom list, as well as running the collision check function
	"""

	# Iterates through the atom_list and moves them according to the velocity stored within its 3rd element
	for atom_object in atom_list:

		# Move the rectangle object stored in list index 1 based on the x,y velocity stored in index 2
		atom_object[1] = atom_object[1].move(atom_object[2])

		# choose a random collision sound to play when something hits a wall
		wall_collision_sound_index = randint(0,len(bounce_audio_list)-1)

		# check if the atom is colliding with a horizontal boundry
		if atom_object[1].left < BORDER or atom_object[1].right > WIDTH-BORDER:
			bounce_audio_list[wall_collision_sound_index].play() # Plays whichever random sound was picked
			atom_object[2][0] = -1 * atom_object[2][0] # This is the x value of the atoms velocity, which is inverted

		# and our verticle boundry check
		if atom_object[1].top < BORDER or atom_object[1].bottom > HEIGHT-BORDER:
			bounce_audio_list[wall_collision_sound_index].play()
			atom_object[2][1] = -1 * atom_object[2][1] # the y velocity of the atom

		check_collisions(atom_object,atom_list) # move on to checking atom collisions for this atom
	return atom_list


def check_collisions(atom_object,atom_list):
	"""
	This function performs collision checking given the specific atom to check, and the atom list itself.

	The collision check is performed by looping through the whole list and checking each item against atom_object.

	If a collision is found, both atoms are removed from the list, the create_new_atom function is called with a given x,y pair, and the
	spawn_explosion_at_point function is called using the same x,y pair passed to create_new_atom
	"""

	# This loop causes a not un-noticable jut in rendering, probably because it literally loops through the entire atom list checking each collision.
	# PyGame is an awkward library though and this is a really clumsy way to achieve the collision goal.
	# Still works as intended though. And now if you go back and run this again you'll see the slight pauses when 2 things collide. Sorry about that.

	# Iterates through the atom_list again, this time we're checking each atom contained within to if it collides with our atom_object
	for atoms in atom_list:

		# If our single atom passed from the move_atoms check collides with the rectangle of an atom within atom_list
		if atom_object[1].colliderect(atoms[1]) and atom_object != atoms:

			# Get the x,y from the atom_objects rectangle bounding box, as well as the x,y of the colliding atoms rectangle. The latter is more for debugging purposes
			atom_object_collision_point_x,atom_object_collision_point_y = atom_object[1][0],atom_object[1][1]
			atom_collision_point_x,atom_collision_point_y = atoms[1][0],atoms[1][1]

			# Get the dimensions of the atom, then calculate its center. This is for later use in preventing new atoms spawning within the boundry wall and bouncing endlessly.
			# The chance of this happening is now significantly less, though not impossible. Edge cases do still exist
			atom_object_width,atom_object_height = atom_object[0].get_size()
			atom_object_center_x, atom_object_center_y = (atom_collision_point_x+(atom_object_width/2)),(atom_collision_point_y+(atom_object_height/2))

			# print out the collision x,y for both atoms (these values are not the collision points, just the top left of each. Pygame is weird (stupid))
			# along with their current indexes in atom_list
			#print "collision at [%d,%d],[%d,%d]! Removing atom %d and %d." %(atom_object_collision_point_x,atom_object_collision_point_y,atom_collision_point_x,atom_collision_point_y,atom_list.index(atom_object),atom_list.index(atoms))

			# remove both of the atoms from the list
			atom_list.pop(atom_list.index(atom_object))
			atom_list.pop(atom_list.index(atoms))

			# spawn a new atom at roughly the center of the atom we were testing collision with
			create_new_atom(atom_object_center_x, atom_object_center_y)

			# now lets spawn an EXPLOSION at that point as well
			spawn_explosion_at_point(atom_object_center_x, atom_object_center_y)

			# break out of our loop because we have nothing left to check for this run, and have found our collision
			break;

def spawn_explosion_at_point(spawn_x,spawn_y):
	"""
	Spawns a random explosion image with a random scale at a given x,y point, and then plays a random 'explosion' sound from the predefined list.
	"""

	# FIRST STEPS OF MAKING AN AMAZING EXPLOSION: PICK ONE AT RANDOM
	random_explosion_image_index = randint(0,len(explosion_image_list)-1)

	# LETS CONVERT_ALPHA ON IT BECAUSE ITS A PNG WITH TRANSPARENCY (PROBABLY)
	explosion_decal = pygame.image.load(explosion_image_list[random_explosion_image_index]).convert_alpha()

	# NEXT STEPS, FIND OUT HOW BIG IT IS, THEN PICK A RANDOM FLOATING POINT VALUE BETWEEN 0.2 AND 0.5 USING UNIFORM() TO SCALE IT WITH
	w,h = explosion_decal.get_size()
	scale = uniform(0.2,0.5)

	# NOW LETS SCALE THAT CONVERTED IMGE FROM BEFORE
	explosion_decal = pygame.transform.scale(explosion_decal, (int(w*scale), int(h*scale)))

	# AND WHILE WE'RE AT IT LETS PLAY AN EXPLOSION SOUND
	audio_list[randint(0,len(audio_list)-1)].play()

	# THEN LETS ADD THIS BAD BOY TO THE LIST OF EXPLOOOOOOSIONS FOR RENDERING LATER
	explosion_pos_list.append([explosion_decal,explosion_decal.get_rect(center = (spawn_x,spawn_y))])




	# EXPLOOOOOOOOOOOOOOOOOOOOSIONS

def main():
	"""
	Main program loop.

	Handles rendering and window event listeners, this loop does not perform any game logic outside of redrawing the canvas or determaining when to end the program
	based on if a quit event is triggered, or spawning a new atom when the mouse is clicked.
	"""

	# The main loop of the 'game', as long as finished is false, all (most) of the above functions will be run, this function mainly runs to handle rendering and redrawing
	finished = False

	# lets build our audio list first
	load_audio()

	# enter the main rendering loop
	while not finished:

		# start fresh with a clean screen of a nice, lovely BACKGROUND_COLOUR
		frame.fill(BACKGROUND_COLOUR)

		# Event listeners for mouse clicks (either right or left), and quitting the game
		for event in pygame.event.get():
			if event.type == pygame.MOUSEBUTTONUP:
				create_new_atom()
			elif event.type == pygame.QUIT: # check for quitting via the close button...
				finished = True
			elif event.type == pygame.KEYDOWN: # ...or we can also quit via alt+f4, left or right, either works.
				if event.key == 285 and (event.mod == 4416 or event.mod == 4672): # 285 is the key code for f4, mod 4416 is left alt, and 4672 is right alt.
					finished = True

		# Call the move_atoms function and update the new positions of all our atoms, test collisions, add new atoms and explosions to their respective lists and play sounds
		move_atoms(atom_pos_list)

		# draw any explosions in the explosion list to the canvas
		for EXPLOSION in explosion_pos_list:
			frame.blit(EXPLOSION[0],EXPLOSION[1])

		# and then draw the atoms next, we don't want them to appear under the explosions, so they are drawn last (and draw order places the last drawn items on top of everything drawn before it)
		for atom in atom_pos_list:
			frame.blit(atom[0],atom[1])

		# Update our canvas with all our new pretty pictures
		pygame.display.flip()

		# Then wait our predetermained DELAY milliseconds before starting all over again.
		pygame.time.wait(DELAY)

	# if finished = true, we'll reach this point and then its auf wiedersehen to our pygame program
	pygame.quit()

# But before we do all that fun stuff, lets create our initial collection of atoms by calling the function endlessly until the for loop ends
for _ in range(randint(3,7)):
	create_new_atom()

# And now lets start the main loop proper.
main()
